//
// Created by E540 on 04/06/2020.
//
#include "ListImplement.h"
#include <stdexcept>

using namespace std;

void ListImplement::insert(int data) { // начало итерирования,
    if (iterator.current_ == nullptr) {
        start_ = new Node(data);
        end_ = start_;
        iterator.current_ = start_;
    } else {
        if (iterator.current_->getNext() == nullptr) {
            Node *newNode = new Node(data, start_);
            start_->setNext(newNode);
            start_ = start_->getNext();
            iterator.current_ = start_;
        } else {
            Node *temp = iterator.current_->getNext();
            Node *newNode = new Node(data, iterator.current_, iterator.current_->getNext());
            iterator.current_->setNext(newNode);
            iterator.current_ = iterator.current_->getNext();
            temp->setPrev(iterator.current_);
        }
    }
    ++size_;
}

void ListImplement::remove();

void ListImplement::find(int data);

void ListImplement::makeEmpty();

bool ListImplement::isEmpty();

int ListImplement::countElement();

Iterator ListImplement::*getFirst();

ListImplement::ListIterator::ListIterator(ListImplement *list) { //итератор привязывается к списку
    list_ = list;
    current_ = nullptr;
}

void ListImplement::ListIterator::start() {
    if (list_->start_ == nullptr) {
        throw logic_error("List is empty");
    }
    current_ = list_->start_;
}

void ListImplement::ListIterator::next() {
    if (list_->start_ == nullptr) {
        throw logic_error("List is empty");
    }
    if (current_->getNext() == nullptr) {
        current_ = current_->getNext();
    }
}

bool ListImplement::ListIterator::isFinished() {
    if (list_->start_ == nullptr) {
        throw logic_error("List is empty");
    }
    return current_->getNext() == nullptr;
}

int ListImplement::ListIterator::get() const{
    if (list_->start_ == nullptr) {
        throw logic_error("List is empty");
    }
    return current_->getData();
}