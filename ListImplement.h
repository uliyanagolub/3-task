//
// Created by E540 on 04/06/2020.
//

#ifndef INC_3PAGE_LISTIMPLEMENT_H
#define INC_3PAGE_LISTIMPLEMENT_H

#include "Iterator.h"
#include "List.h"
#include "Node.h"

class ListImplement : public List {
private:

    class ListIterator : public Iterator {
    private:
        ListImplement *list_;
        Node *current_;
    public:
        ListIterator(ListImplement *list);

        void start(); // начать перебор элементов

        void next(); // перейти к следующему элементу

        bool isFinished(); // проверка, все ли проитерировано

        int get() const; // получить очередной элемент очереди

        friend class ListImplement;

    };

    Node *start_, *end_;
    ListIterator iterator;
    int size_;

public:
    friend class ListIterator;

    ListImplement(int size);

    ListImplement(Node *head, Node *tail, ListIterator iterator, int size);

    void insert(int data) override;

    void remove();

    void find(int data);

    void makeEmpty();

    bool isEmpty();

    int countElement();

    Iterator *getFirst();


    void start(); // методы друга

    void next();

    bool isFinished();

    int get() const;
};


#endif //INC_3PAGE_LISTIMPLEMENT_H
