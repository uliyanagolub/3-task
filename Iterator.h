//
// Created by E540 on 04/06/2020.
//

#ifndef INC_3PAGE_ITERATOR_H
#define INC_3PAGE_ITERATOR_H



class Iterator {
public:
    virtual void start() = 0; // начать перебор элементов

    virtual void next() = 0; // перейти к следующему элементу

    virtual bool isFinished() = 0; // проверка, все ли проитерировано

    virtual int get() const = 0; // получить очередной элемент очереди

};

#endif //INC_3PAGE_ITERATOR_H
