//
// Created by E540 on 06/06/2020.
//

#include "Node.h"

Node::Node(int data, Node *next = nullptr, Node *prev = nullptr) {
    data_ = data;
    next_ = next;
    prev_ = prev;
}

Node::Node(int data, Node *prev = nullptr){ // конструктор по предыдущему
    data_ = data;
    next_ = nullptr;
    prev_ = prev;
}

Node::Node(int data){  // конструктор по дате
    data_ = data;
    next_ = nullptr;
    prev_ = nullptr;
}

Node::~Node(){
    next_ = nullptr;
    prev_ = nullptr;
}

void Node::setPrev(Node *prev){
    prev_= prev;
}

void Node::setNext(Node *next){
    next_ = next;
}

Node *Node::getPrev() const {
    return prev_;
}

Node *Node::getNext() const {
    return next_;
}

int Node::getData() const{
    return data_;
}