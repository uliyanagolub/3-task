//
// Created by E540 on 04/06/2020.
//

#ifndef INC_3PAGE_LIST_H
#define INC_3PAGE_LIST_H

#include "Iterator.h"

class List {

public:

    virtual void insert(int data) = 0;

    virtual void remove() = 0;

    virtual void find(int data) = 0;

    virtual void makeEmpty() = 0;

    virtual bool isEmpty()= 0;

    virtual int countElement() = 0;

    virtual Iterator *getFirst() = 0; //индекс первого элемента

};


#endif //INC_3PAGE_LIST_H
