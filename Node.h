//
// Created by E540 on 06/06/2020.
//

#ifndef INC_3PAGE_NODE_H
#define INC_3PAGE_NODE_H

struct Node {
    int data_;
    Node *next_, *prev_;

public:
    Node(int data, Node *prev, Node *next); //конструктор по всему

    Node(int data, Node *prev); // конструктор по предыдущему

    Node(int data); // конструктор по дате

    ~Node();

    void setPrev(Node *prev);

    void setNext(Node *next);

    Node *getPrev() const;

    Node *getNext() const;

    int getData() const;
};



#endif //INC_3PAGE_NODE_H
